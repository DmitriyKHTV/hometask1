#include <iostream>
#include<ctime>
#include <iomanip>
#include <cmath>

#include <conio.h>

using namespace std;


int matrix[10][10]{};
void pause();
int randomNumber(); 
void fillMatrix();
void showMatrix();
void taskA();
void taskB();
void taskC(); 
void menu(int taskNumber = 0, bool continueProgram = 1);

int main() {
	srand(time(0));
	fillMatrix();
	menu();
	return 0;
}

void pause()
{
	cout << "\n\nPress any key to continue...";
	_getch();
	system("cls");
}

int randomNumber()
{
	int answer = 1 + rand() % 20;
	return answer;
}

void fillMatrix()
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			matrix[i][j] = randomNumber();
		}
	}
}

void showMatrix()
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			cout <<setw(4)<< matrix[i][j] << "  ";
		}
		cout << "\n";
	}
}
//
void taskA()
{
	cout << "the matrix is:\n";
	showMatrix();
	for (int i = 0; i < 10; i++)
	{
		int minimum = 21, maximum{}, minimumJ{}, maximumJ{};
		for (int j = 0; j < 10; j++)
		{
			if (matrix[i][j] < minimum)
			{
				minimum = matrix[i][j];
				minimumJ = j;
			}
			if (matrix[i][j] >= maximum)
			{
				maximum = matrix[i][j];
				maximumJ = j;
			}
		}
		if (maximumJ < minimumJ)
		{
			swap(minimumJ, maximumJ);
		}
		int copyMatrix[10]{};
		for (int h = 0, k = 0; h < 10; h++, k++)
		{
			copyMatrix[h] = matrix[i][k];
		}
		int f = maximumJ - 1;
		for (int j = minimumJ + 1; j < maximumJ; j++)
		{
			matrix[i][j] = copyMatrix[f--];
		}

	}
	cout << "\n\nchanged matrix:\n";
	showMatrix();
}

void taskB() {
	cout << "The matrix is:\n";
	showMatrix();
	bool flag = true;
	int i{};
	int j{};
	while (i != 4) {
		int k = 9 - i;
		int l = 9 - j;
		if (matrix[i][j] != matrix[k][l]) 
		{
			flag = false;
		}
		i++;
		j++;
	}
	if (flag == true) {
		cout << "\n\ntask 2 +" << endl;
	}
	else {
		cout << "\n\ntask 2 -" << endl;
	}
}

void taskC() {
	cout << "The matrix is:\n";
	showMatrix();
	int theSumm{};
	for (int i = 1; i < 9; i++) {
		for (int j = 1; j < 9; j++) {
			if (matrix[i][j] > 0) {
				theSumm += matrix[i][j];
			}
		}
	}
	cout << "\n\nThe summ is " << theSumm << endl;
}

void menu(int taskNumber, bool continueProgram)
{
	while (continueProgram) {
		cout << "The matrix is:\n";
		showMatrix();
		cout << "\n\nChoose task:\n\n1)change the order in a raw\n2)check diagonal for symmetry\n3)summ of positive elements\n0)exit\n\nEnter the number of task: ";
		cin >> taskNumber;
		system("cls");
		switch (taskNumber)
		{
		case 1:
			taskA();
			pause();
			break;
		case 2:
			taskB();
			pause();
			break;
		case 3:
			taskC();
			pause();
			break;
		default:
			continueProgram = 0;
			break;
		}
	}

}